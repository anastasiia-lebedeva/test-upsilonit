import re


def get_image_link(_lot: dict) -> str:
    """
    Получение ссылки на изображение. Если в `img_file_name` нет названия изображения, то возвращает пустую строку

    :param _lot: Словарь, имеющий поля `auc_num` и `img_file_name`
    :return: Строка с ссылкой на изображение
    """
    if _lot['img_file_name'] is not None or len(_lot['img_file_name']) > 0:
        return ('https://images.k-auction.com/www/Work/'
                + str(_lot['auc_num']).zfill(4)
                + '/T/'
                + str(_lot['img_file_name']))
    return ''


def get_image_size(_lot: dict) -> tuple:
    """
    Размер лота проверяется в полях `size_length` и `size_width`. Если в этих полях не обнаружены значения, \
    то размер парсится из поля `size` (если в этом поле фигурирует `diameter`, то оба значения будут равны первому \
    найденному значению). Если на выходе получен кортеж с длиной, не равной 2, то возвращает (-1, -1).

    :param _lot: словарь(json) с полями size_length, size_width / size
    :return: кортеж из двух значений с размерами лота или (-1, -1)
    """
    try:
        image_size = (_lot['size_length'], _lot['size_width'])
        if '' in image_size or None in image_size:
            image_size = re.findall(r'[\d.]+', _lot['size'])
            if 'diameter' in _lot['size']:
                image_size *= 2
        return tuple(map(float, image_size)) if len(image_size) == 2 else (-1., -1.)
    except ValueError:
        return -1., -1.
