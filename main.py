import json  # Parse POST response

import psycopg2  # Connect to PostgreSQL
import requests  # Get html pages

from data_extract import data_extract

query_create_table = '''
create table if not exists auction (
    auction_id   int,      -- Номер аукциона    
    auction_date date,     -- Дата аукциона     
    lot_id       int,      -- Номер лота        
    lot_name     varchar,  -- Название лота     
    author       varchar,  -- Автор             
    image_link   varchar,  -- Ссылка на картинку
    image_size   float[2], -- Размер картины    
    constraint pk_auction
        primary key (auction_id, lot_id)
)
'''

query_insert = '''
insert into auction
values (%s, %s, %s, %s, %s, %s, '{{{0}, {1}}}');
'''

conn = psycopg2.connect(
    host='localhost',
    port='6000',
    database='upsilon_it_test',
    user='postgres',
    password='123456'
)
cur = conn.cursor()
cur.execute(query_create_table)
conn.commit()

# get all auction's data: ids, work count and dates
all_auctions_page = 1
auctions = list()
# iterating because of pagination
while True:
    p = requests.post('https://www.k-auction.com/api/Auction/Schedule/Major',
                      json={'page': str(all_auctions_page), 'search': ''})
    if p.status_code != 200 or len(auctions_on_page := json.loads(p.content.decode(encoding='utf-8'))['data']) == 0:
        break
    auctions += auctions_on_page
    all_auctions_page += 1

for auction in auctions:
    # get data for db
    auction_id = auction['auc_num']
    auction_date = auction['auc_date']

    # for each auction get  lots in itself page
    lots = requests.post('https://www.k-auction.com/api/Auction/' + auction['auc_kind'] + '/' + str(auction_id),
                         json={'page_size': str(auction['work_count']),
                               'auc_num': str(auction_id),
                               'auc_kind': auction['auc_kind']}).content
    lots = json.loads(lots.decode(encoding='utf-8'))['data']

    for lot in lots:
        lot_id = lot['lot_num']
        lot_name = lot['title']
        author = lot['artist_name']
        image_link = data_extract.get_image_link(lot)
        image_size = data_extract.get_image_size(lot)

        # insert row to db if it not exists
        try:
            cur.execute(query_insert.format(*image_size),
                        (auction_id, auction_date, lot_id, lot_name, author, image_link))
            conn.commit()
        except psycopg2.Error:
            pass

cur.close()
conn.close()
